
<?php include 'DBManager.php'; ?>
<?php include 'SQLDataTypes.php'; ?>

<?php
/**
 * Created by PhpStorm.
 * User: zero
 * Date: 10/25/14
 * Time: 4:02 PM
 *
 * Model class is used to represent a single record in a database table.
 */




abstract class BaseModel {

    private $id=-1;
    private $tableName;
    private $className;
    private $tableColValPair; //store column value pair for definition of table,
                              // e.g ['Name', 'VARCHAR(50)', 'Age', 'INT']
    private $SELECT_ALL_QUERY;//store constant query for selecting all records

    static public $dbManager = NULL; //all models use single database connection
    protected $tableColumns;
    /**PUBLIC METHODS **/

    public function __construct()
    {
        $this->className = get_class( $this );
        $this->tableName = ucfirst( $this->className ) . "s";//create table name from object class name
        if(BaseModel::$dbManager == NULL){
            BaseModel::$dbManager = new DBManager(DB_HOST, DB_NAME, DB_USER, DB_PASS);
        }
        $this->tableColValPair = $this->createTableColArray();
        $this->createTable();
        $this->createMethods();
        $this->tableColumns = array('id');

        $query = 'SELECT';
        $colCount = 0;
        $len = count( $this->tableColValPair );
        foreach( $this->tableColValPair as $column ){
            $colCount ++;
            if ($colCount %2 != 0){
                $this->tableColumns = array_merge($this->tableColumns, array($column));
                $query .= " $column";
                if ( $colCount < $len -2)
                    $query .= ", ";
            }
        }
        $query .= ", id FROM $this->tableName;";
        $this->SELECT_ALL_QUERY = $query;
    }


    public function __call( $method, $args ){

        $className = get_class( $this );
        $args = array_merge( array( $className => $this ), $args );

        if ( isset( $this->{$method} ) && is_callable( $this->{$method} ) ){
            return call_user_func( $this->{$method}, $args );
        } else {
            throw new Exception("Fatal error: Call to undefined method $className::{$method}()");
        }
    }


    //*SEARCH METHODS*//
    //Note: These return an array of models, containing
    //      objects created from the sql result using
    //      the 'createModelsFromResult()' method

    public function findByID( $id ){

        $assoc['id'] = $id;
        return $this->inclusiveSearch( $assoc );
    }

    public function getID(){
        return $this->id;
    }

    //prepare select part of sql query
    private function prepQuery($assoc){
        
        $query = str_replace(";", "", $this->SELECT_ALL_QUERY);
        //echo "$query\n";
        $query .= BaseModel::$dbManager->searchQuery($assoc);
        return $query;
    }

    //search database table using 'AND' to join multiple
    //column searches
    public function inclusiveSearch( $assoc ){
        if (sizeof($assoc) < 1 || $assoc == NULL){
            return $this->getAll();
        }
        $query = $this->prepQuery($assoc);
        $query = str_replace("~LINK~", "AND", $query);
        $results = BaseModel::$dbManager->sqlQuery( $query );
        $models = $this->createModelsFromResult( $results );

        return $models;
    }


    //search database table using 'OR' to join multiple
    //column searches
    public function exclusiveSearch( $assoc ){

        if (sizeof($assoc) < 1 || $assoc == NULL){
            return $this->getAll();
        }
        $query = $this->prepQuery($assoc);
        $query = str_replace("~LINK~", "OR", $query);
        $results = BaseModel::$dbManager->sqlQuery($query);
        $models = $this->createModelsFromResult( $results );

        return $models;
    }


    //save current object into record in database table
    public function save(){

        $assoc_new = $this->createAssoc();
        $models = array();

        if ( $this->id == -1 ) {
            if ( $this->getAssocNullCount($assoc_new) == 0 ){
                $models = $this->inclusiveSearch($assoc_new);
                //echo "inserting\n";
                return (BaseModel::$dbManager->insert($assoc_new) != false );
            }
        } else {
            $models = $this->searchWhereID( $this->id );
            //if exists, update otherwise insert
            if ( count( $models ) > 0 ) {
                //echo "updating..\n";
                $assoc = $models[0]->createAssoc();
                return (BaseModel::$dbManager->inclusiveUpdate($assoc, 
                            $assoc_new) != false);
            }
        }
        foreach($assoc_new as $arg => $val){
            echo "$arg => $val\n";
        }
        //echo "Save failed.\n";

    }

    //remove current object record from database table
    public function delete(){

        if ( $this->modelExists() ) {
            $assoc = $this->createAssoc();
            return ( BaseModel::$dbManager->inclusiveDelete($assoc) != false );
        }
        return true;
    }


    //check if a model with the current object's assoc array( record data )
    // exists
    public function modelExists(){

        $assoc = $this->createAssoc();
        $models = $this->inclusiveSearch( $assoc );

        return ( count($models)> 0 );
    }

    //get model array containing all models in database
    public function getAll(){

        //using 'SELECT *' is not as efficient as using the column names
//        $result = BaseModel::$dbManager->sqlQuery( "SELECT * FROM $this->tableName;" ); //old
        $result = BaseModel::$dbManager->sqlQuery( $this->SELECT_ALL_QUERY );
        $models = $this->createModelsFromResult($result);


        return $models;
    }

    //run raw sql query and get array of models in return
    public function sqlQuery( $query ){

        $result = BaseModel::$dbManager->sqlQuery( $query );
        $models = $this->createModelsFromResult($result);

        return $models;
    }

    //run raw sql query and get raw sql result
    public function sqlQueryRaw( $query ){
        return BaseModel::$dbManager->sqlQuery($query);
    }

    /**PROTECTED METHODS **/

    protected function getTableName(){
        return $this->tableName;
    }

    public function getAbsoluteUrl(){
        return null;
    }

    //create assoc array of class's public variables
    abstract protected function createAssoc();

    //get array of tableColumns and type values
    abstract protected function createTableColArray();

    abstract protected function getAssocNullCount( $assoc );

    /**PRIVATE METHODS **/

    //create setters, getters and search Methods dynamically
    private  function createMethods()
    {

        foreach ($this as $attr_name => $value) {

            # if attribute is not a function
            if (!$value instanceof Closure) {
                $this->{"set" . ucfirst($attr_name)} = function ($args) use ($attr_name) {
                    $this->$attr_name = $args[0];
                };

                $this->{"get" . ucfirst($attr_name)} = function () use ($attr_name) {
                    return $this->$attr_name;
                };

                $this->{"findBy" . ucfirst($attr_name)} = function ($args) use ($attr_name) {
                    $assoc[$attr_name] = $args[0];
                    return $this->exclusiveSearch($assoc);
                };
            }
        }
    }

    //creates a set of model objects based on mysql query result
    private function createModelsFromResult( $result ){

        $models = array();
        while( $row = $result->fetch_assoc() ){

            $model = new $this->className();
            foreach( $row as $col => $value ) {
                $model->{$col} = $value;
            }
            array_push( $models, $model );
        }

        return $models;
    }

    //create a mysql db table based on public variables of class
    private function createTable()
    {
        $tbl_cols = $this->tableColValPair;
        $query = "CREATE TABLE IF NOT EXISTS $this->tableName (id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,";
        $len = count($tbl_cols);
        for ( $i =0; $i < $len; $i+=2 ){
            $query .= $tbl_cols[$i];
            $query .= " " . $tbl_cols[$i + 1];
            $query .= ", ";
        }

        ///remove unnecessary ','
        $query= trim($query);
        $len = strlen($query);
        if ( $query[ $len-1] == ',') {
            $query[$len - 1] = " ";
        }
        $query= trim($query);
        $query .= " );";
//        echo "$query\n\n\n";
        BaseModel::$dbManager->sqlQuery($query);
    }

}


class Model extends BaseModel{

    /**PUBLIC METHODS **/
    public function __construct($assoc_var = array())
    {
        parent::__construct();
        //set method fields using assoc data
        foreach( $assoc_var as $var_name => $value ){
            $this->{"set" . ucfirst($var_name)}($value);
            //echo "$var_name => $value\n";
        }
    }


    /**PROTECTED METHODS **/

    //create array containing database table columns
    //and column types
    final protected function createTableColArray()
    {
        $tableCols = array();
        foreach( $this as $var_name => $val ){
            $obj = $this->{$var_name};
            if( is_object($obj) ) {
                # add attribute as table column if it is child of BaseSQLDataType 
                # or BaseDBrel. done to prevent adding unnecessary table columns
                if (get_parent_class($obj) == 'SQLDataType\BaseSQLDataType') {
                    $tableCols = array_merge($tableCols, array($var_name, $val));
                    $this->{$var_name} = null;
                }else if (get_parent_class($obj) == 'SQLDataType\BaseDBRel'){
                    $tableCols = array_merge($tableCols, array($var_name, $val));
                    $this->{$var_name} = null;
                    $this->{'getRelated' . ucfirst(
                        $obj->getRelatedTableName())} = 
                        function () use ($obj, $var_name){
                            # returns related Model objects
                            $id = $this->getID();
                            # if object is in db
                            if ( $id != -1 ){
                                $relID = $this->{$var_name};
                                echo "relID => $relID\n";
                                $relObj = new $obj->getRelatedTableName();
                                return $relObj->findByID($relID);
                            }
                        };
                    $this->{'setRelated' . ucfirst(
                        $obj->getRelatedTableName())} = 
                        function ($args) use ($obj, $var_name){
                            # set related object
                            $id = $args[0]->getID();
                            if ($id != -1){
                                $this->{$var_name} = $id;
                            }
                            echo "id => $id\n";
                        };
                }
            }
        }

        return $tableCols;
    }

    protected function getAssocNullCount( $assoc ){

        $count = 0;
        foreach( $assoc as $var_name => $value  ){
            if ( $value == null )
                $count ++;
        }

        return $count;
    }

    //create assoc array from public var names and values.
    //this is model's assoc array
    protected function createAssoc(){

        $assoc['__table__'] = $this->getTableName();
        //place every public variable in array
        foreach( $this as $col => $value ){
            # only valid table columns used in assoc array
            $valid = false;
            foreach($this->tableColumns as $item){
                if ($item == $col){
                    $valid = true;
                    break;
                }
            }
            if (!$value instanceof Closure and $valid) {
                $assoc = array_merge( $assoc, array($col => $value) );
                //echo "$col => $value\n";
            }
        }

        return $assoc;
    }
}


/***
 * Class User
 *
 * Generic User Model, username, email, password
 */
class User extends Model{

//    public $name = 'VARCHAR(50) NOT NULL';
    public $username;
    public $email;
    public $password;

    public function __construct($assoc = array()){
        $this->username = new SQLDataType\VARCHAR(array('length'=>20));
        $this->email = new SQLDataType\VARCHAR(array('length'=>40));
        $this->password = new SQLDataType\VARCHAR(array('length'=>60));
        parent::__construct($assoc);
        # hash password
        //$this->password = md5($assoc['password']);
    }

    # validate username|password combination
    public function isValidUserPass( $username, $password ){

        $assoc['username'] = $username;

        $models = $this->exclusiveSearch( $assoc );
        if ( count( $models ) > 0 ){
            return ( $models[0]->password == md5($password) );
        }

        return false;
    }

    public function setPassword($password){
        $this->password = md5($password);
    }

    # validate email|password combination
    public function isValidEmailPass( $email, $password ){

        $assoc['email'] = $email;

        $models = $this->exclusiveSearch( $assoc );
        if ( count( $models ) > 0 ){
            return ( $models[0]->password == md5($password) );
        }

        return false;
    }

    # send email to user
    public function mailUser($sender, $subject, $msg){
        if($this->email != NULL){
            mail($this->email, $subject, $msg, $sender);
        }
    }
}
?>
