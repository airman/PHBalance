<?php
/**
 * User: zero
 * Date: 10/25/14
 * Time: 4:07 PM
 *
 *
 * Handles basic database transactions
 */

require_once('Setup.php');

class DBManager {

    private $dbHost;
    private $dbPass;
    private $dbUser;
    private $dbName;
    private $link;

    function __construct($dbHost, $dbName, $dbUser, $dbPass)
    {
        $this->dbHost = $dbHost;
        $this->dbName = $dbName;
        $this->dbUser = $dbUser;
        $this->dbPass = $dbPass;
        $this->connect();
    }

    function __destruct(){

        $this->disconnect();
    }

    public function getLink()
    {
        return $this->link;
    }

    public function disconnect(){

        mysqli_close( $this->link );
    }

    private function connect( ){

        $this->link = mysqli_connect( $this->dbHost, $this->dbUser, $this->dbPass, $this->dbName );
        if ( !$this->link )
            die( "Connection Failure:". $this->link->connect_error );
    }

    private function checkForTable($assoc, $func_name){
        if ( !isset( $assoc['__table__'] ) ) {
            die('Fatal: $assoc[\'__table__\'] not found in array '
                ."DBManager::$func_name()");
        }
    }

    //returns generic string for simple sql search statement
    public function searchQuery($assoc){
        if (sizeof($assoc) < 1){
            die('Fatal: Not enough values in array passed to' 
                .'DBManager::searchQuery()');
        }
        $query = ' WHERE ';
        $i = 1;
        foreach ($assoc as $col => $value) {

            //skip $assoc['__table__']
            if ( $col == '__table__' || $value == NULL) {
                $i++;
                continue;
            }
            $query .= " $col = ";
            if (gettype($value) == 'string') {
                // escape strings
                $query .= "'" . 
                    mysqli_real_escape_string($this->link, $value) . "'";
            } else{
                $query .= "$value";
            }

            if ($i < count($assoc) ) {
                $query .= " ~LINK~ ";
            }
            $i++;
        }

        $query .= " ;";
        return $query;
    }

    //select query using 'or' e.g exclusiveSearch( array( '__table__' => 'dbTable1', 'col1' => 56 ) );
    public function exclusiveSearch( $assoc ){
        $this->checkForTable($assoc, 'exclusiveSearch');
        $query = "SELECT * FROM " . $assoc['__table__']. " " . $this->searchQuery( $assoc );
        $query = str_replace( "~LINK~", "OR", $query );

        return $this->sqlQuery( $query );
    }

    //select query using and
    public function inclusiveSearch( $assoc )
    {
        $this->checkForTable($assoc, 'inclusiveSearch');
        $query = "SELECT * FROM " . $assoc['__table__']. " " . $this->searchQuery( $assoc );
        $query = str_replace( "~LINK~", "AND", $query );

        return $this->sqlQuery( $query );
    }

    //delete query using 'and'
    public function inclusiveDelete( $assoc )
    {
        $this->checkForTable($assoc, 'inclusiveDelete');
        $query = "DELETE FROM " . $assoc['__table__']. " " . $this->searchQuery( $assoc );
        $query = str_replace( "~LINK~", "AND", $query );

        return $this->sqlQuery( $query );
    }

    //delete query using 'or'
    public function exclusiveDelete( $assoc )
    {
        $this->checkForTable($assoc, 'exclusiveDelete');
        $query = "DELETE FROM " . $assoc['__table__']. " " . $this->searchQuery( $assoc );
        $query = str_replace( "~LINK~", "OR", $query );

        return $this->sqlQuery( $query );
    }


    //update query using 'and'
    public function inclusiveUpdate( $assoc, $assoc_new ){
        $this->checkForTable($assoc, 'inclusiveUpdate');
        $sqlUpdate = "UPDATE " . $assoc_new['__table__']. " SET " . $this->searchQuery( $assoc_new );
        $sqlUpdate = str_replace( "WHERE", "", $sqlUpdate );
        $sqlUpdate = str_replace( "~LINK~", ", ", $sqlUpdate );
        $sqlUpdate = str_replace( ";", " ", $sqlUpdate );

        $sqlSearch = $this->searchQuery($assoc);
        $sqlSearch = str_replace( "~LINK~", "AND", $sqlSearch );

        $query= $sqlUpdate . $sqlSearch;
        return $this->sqlQuery( $query );
    }

    //update query using 'or'
    public function exclusiveUpdate( $assoc, $assocNew ){
        $this->checkForTable($assoc, 'exclusiveUpdate');
        $sqlUpdate = "UPDATE " . $assocNew['__table__']. " SET" . $this->searchQuery( $assocNew );
        $sqlUpdate = str_replace( "WHERE", "", $sqlUpdate );
        $sqlUpdate = str_replace( "~LINK~", ", ", $sqlUpdate );
        $sqlUpdate = str_replace( ";", " ", $sqlUpdate );

        $sqlSearch = $this->searchQuery($assoc);
        $sqlSearch = str_replace( "~LINK~", "OR", $sqlSearch );

        $query= $sqlUpdate . $sqlSearch;
        return $this->sqlQuery( $query );
    }

    //database insert
    public function insert( $assoc ){

        if (sizeof($assoc) < 2){
            die('Fatal: Not enough values in array passed to' 
                .'DBManager::insert()');
        }
        if ( !isset( $assoc['__table__'] ) ) {
            die('Fatal: $assoc[\'__table__\'] not found in array '
                .'DBManager::insert()');
        }
        $i =1;
        $cols = '( ';
        $vals = '( ';

        foreach( $assoc as $col => $val  ){
            //ignore assoc['__table__']
            if ( $col == '__table__' || $val == NULL ) {
                $i++;
                continue;
            }
            $cols .= $col ;
            if ( gettype( $val ) == 'string')
                // escape strings
                $vals .= "'" . 
                    mysqli_real_escape_string($this->link, $val) . "'";
            else
                $vals .= $val ;

            if ( $i < count( $assoc ) ){
                $cols .= ', ';
                $vals .= ', ';
            }
            $i ++;
        }
        $cols .= ' ) ';
        $vals .= ' ) ';
        $query = 'INSERT INTO ' . $assoc['__table__'] . " $cols VALUES $vals ;";
        return $this->sqlQuery( $query );
    }

    public function sqlQuery($query)
    {
        $query = trim($query);
        //echo "$query\n\n---------------------------------\n\n";
        $result = mysqli_query( $this->link, $query) or die($this->link->error.__LINE__);

        if($result )
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
}

?>
